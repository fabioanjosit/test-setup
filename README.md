# Setup de test

## Setup Windows

### Softwares recomendáveis para automação de testes Web e Mobile(Android)
- [x] JDK/JRE 1.8
- [x] AndroidStudio
- [x] ApacheMaven
- [x] Eclipse
- [x] Appium
- [x] Chromedriver
- [x] gitbash
- [x] insomnia(TestAPI_Rest)

### Variáveis de Ambiente
```
JDK/JRE 1.8
    JAVA_HOME=C:\Program Files\Java\jdk1.8.0_xxx

ApacheMaven
    MAVEN_HOME=C:\apache-maven-x.x.x

AndroidStudio
    ANDROID_HOME=C:\Users\User\AppData\Local\Android\sdk
    ANDROID_PTOOLS=C:\Users\User\AppData\Local\Android\sdk\platform-tools
    ANDROID_TOOLS=C:\Users\User\AppData\Local\Android\sdk\tools

PATH
    %JAVA_HOME%\bin
    %ANDROID_HOME%
    %ANDROID_HOME%\tools\
    %ANDROID_HOME%\platform-tools\
    %ANDROID_TOOLS%\bin\
    %MAVEN_HOME%\bin
```

### Comandos executados usando um "uiautomatorviewer.bat" para iniciar o uiautomatorviewer
- [x] uiautomatorviewer.bat
```
echo "Iniciando uiautomatorviewer"
C:
cd %ANDROID_HOME%\tools
uiautomatorviewer
timeout /t 30

```

### Comandos de exemplo executados com "Emulador_CelularAndroid8.bat", para iniciar um celular virtual do AndroidStudio
- [x] Emulador_CelularAndroid8.bat
```
echo "Iniciando Nexus_5X_API26_android8"
C:
cd %ANDROID_HOME%\tools
emulator -list-avds
adb devices
emulator @Nexus_5X_API26_android8
timeout /t 30
```


## Setup Linux (em breve)...
